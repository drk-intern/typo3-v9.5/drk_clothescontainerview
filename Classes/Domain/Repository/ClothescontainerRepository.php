<?php

namespace DRK\DrkClothescontainerview\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;

class ClothescontainerRepository extends AbstractDrkRepository
{

    /**
     * @param $sOrgCode
     *
     * @return array
     * @throws NoSuchCacheException
     * @throws \Exception
     */
    public function getClothescontainerList($sOrgCode): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainerview|getClothescontainerList', $sOrgCode]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $params = array(
            'orgcode' => $sOrgCode
        );

        $this->getJsonClient('old');

        $response = $this->jsonClient->getclothcontainerlist($params);

        if (!empty($response)) {
            if (strtolower($response->status) != "ok") {
                throw new \Exception("Der Webservice meldet: " . $response->message);
            } else {
                $clothescontainers = $response->results;
            }
        } else {
            throw new \Exception('Webservice antwortet nicht oder fehlerhaft!');
        }

        // cleanup CC array
        $aCC = array();

        if (!empty($clothescontainers) && is_array($clothescontainers)) {
            foreach ($clothescontainers as $aCCtmp) {
                // without coordinations, not usable
                if (intval($aCCtmp->containerlatitude) > 0 && intval($aCCtmp->containerlongitude) > 0) {
                    $aCC[] = $aCCtmp;
                }
            }

            //now set offer at the end
            foreach ($clothescontainers as $aCCtmp) {
                // without coordinations, not usable
                // an offer has an offerlatitude & offerlongitude, but no containerlatitude & containerlongitude
                if ((intval($aCCtmp->offerlatitude) > 0 && intval($aCCtmp->offerlongitude) > 0) && (intval($aCCtmp->containerid) == 0)) {
                    $aCC[] = $aCCtmp;
                }
            }
        }

        $this->getCache()->set($cacheIdentifier, $aCC, [], $this->heavy_cache);

        return $aCC;
    }

    /**
     * @param string $orgType
     * @param string $sortingBy
     * @param string $orderBy
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function getOrganisationList(string $orgType = 'K', string $sortingBy = null, string $orderBy = null): array
    {
        $cacheIdentifier = sha1(json_encode(['drk_clothescontainerview|getOrganisationList', $orgType, $sortingBy, $orderBy]));
        $result = $this->getCache()->get($cacheIdentifier);

        if (($result !== null) && ($result !== false)) {
            return $result;
        }

        $organisations = Utility::convertObjectToArray(
            $this->getDldbClient()->getOrganisationList($orgType, $sortingBy, $orderBy)
        );

        if (empty($organisations)) {
            return [];
        }

        $this->getCache()->set($cacheIdentifier, $organisations, [], $this->heavy_cache);

        return $organisations;
    }

    /**
     * ItemsProcFunc for Clothescontainerview->settings.flexforms.preselection_organisation
     *
     * @param array $parameters
     *
     * @return array
     * @throws NoSuchCacheException
     */
    public function addOrganisationListItems(array $parameters): array
    {
        $this->initSettingsForListItems($parameters);
        // get LV
        foreach ($this->getOrganisationList('L') as $organisation) {
            $parameters['items'][] = [
                "LV " . $organisation['orgName'],
                $organisation['orgID'],
                '',
            ];
        }

        //get KV
        foreach ($this->getOrganisationList() as $organisation) {
            $parameters['items'][] = [
                "KV " . $organisation['orgName'],
                $organisation['orgID'],
                '',
            ];
        }

        return $parameters;
    }

}
