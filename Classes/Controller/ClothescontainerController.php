<?php

namespace DRK\DrkClothescontainerview\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 André Gyöngyösi <a.gyoengyyoesi@drkserivce.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkClothescontainerview\Domain\Repository\ClothescontainerRepository;

/**
 * Class ClothescontainerController
 * @package DRK\DrkClothescontainerview\Controller
 */
class ClothescontainerController extends AbstractDrkController
{
    /**
     * @var ClothescontainerRepository
     */
    protected $ClothescontainerRepository;

    /**
     * Init
     *
     * @param ClothescontainerRepository $ClothescontainerRepository
     * @return void
     */
    public function injectClothescontainerRepository(ClothescontainerRepository $ClothescontainerRepository)
    {
        $this->ClothescontainerRepository = $ClothescontainerRepository;
    }

    /**
     * index view
     */
    public function indexAction(): \Psr\Http\Message\ResponseInterface
    {
        parent::initializeAction();

        $sOrgCode = $this->settings['preselection_organisation'] ??= '';

        if (!empty($sOrgCode)) {
            $clothescontainers = $this->ClothescontainerRepository->getClothescontainerList($sOrgCode);
        }
        else {
            $clothescontainers = [];
        }

        $this->view->assignMultiple([
            'clothescontainers' => $clothescontainers
        ]);

        return $this->htmlResponse();
    }
}
