<?php

// Clothescontainerview Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_clothescontainerview',
    'Clothescontainerview',
    'LLL:EXT:drk_clothescontainerview/Resources/Private/Language/locallang_be.xlf:tt_content.clothescontainer_plugin.title',
    'EXT:drk_clothescontainerview/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kleidercontainer'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // FlexForm configuration schema file
    'FILE:EXT:drk_clothescontainerview/Configuration/FlexForms/Clothescontainerview.xml',
    // ctype
    'drkclothescontainerview_clothescontainerview'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers,
            --div--;Ansicht,
            pi_flexform'
    ]
);
