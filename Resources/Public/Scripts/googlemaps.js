require(['jquery', google_maps_src], function ($) {
  'use strict';

    let icon_cc = "/typo3conf/ext/drk_clothescontainerview/Resources/Public/Images/pic_marker_cc.png";
    let icon_cs = "/typo3conf/ext/drk_clothescontainerview/Resources/Public/Images/pic_marker_cs.png";
    let icon_w = "/typo3conf/ext/drk_clothescontainerview/Resources/Public/Images/pic_marker_w.png";

    let map = new google.maps.Map(document.getElementById("map"), {
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      maxZoom: 17
    });

    let bounds = new google.maps.LatLngBounds();

    function placeMarker(location) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(location[1], location[2]),
        title: location[0],
        icon: '//' + window.location.hostname + eval(location[3]),
        map: map
      });
      let infowindow = new google.maps.InfoWindow({
        content: location[4]
      });

      marker.addListener("click", function () {
        infowindow.open(map, marker);
      });

      bounds.extend(marker.position);
    }

    let i = 0;
    for (i = 0; i < locations.length; i++) {
      placeMarker(locations[i]);
    }
    map.fitBounds(bounds);
});
