<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'drk_clothescontainerview',
    'Clothescontainerview',
    [
        \DRK\DrkClothescontainerview\Controller\ClothescontainerController::class => 'index',

    ],
    // non-cacheable actions
    [
        \DRK\DrkClothescontainerview\Controller\ClothescontainerController::class => 'index',

    ],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

